#' Function to create DEM class object
#'
#' This function creates an object of class DEM using a matrix given
#'
#' @param M numeric matrix
#' @return object of class DEM
#' @export
DEM <- function(M) {
  if(class(M) == "matrix") {
    if(is.numeric(M)) {
      class(M) <- append(class(M),"DEM")
      return(M)
    } else {
      print("Matrix is not numeric.")
    }
  } else {
    print("Object is not a matrix.")
  }
}
