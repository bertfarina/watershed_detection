#' Plot a numeric matrix using Rainbow colors.
#'
#' \code{colorPlotRainbow} returns a plot of the DEM with rainbow colors.
#'
#' This is one of the three \code{colorPlot} functions to visualize a DEM.
#'
#' @param W DEM object.
#' @param numColors An integer for the number of different colors in the plot.
#' @return A plot of the \code{W} DEM using a range of \code{numColors} different colors of the R palette Rainbow.
#'
#' @examples
#' colorPlotRainbow(dataTest, 64)
#' colorPlotRainbow(dataTest, 32)
#' colorPlotRainbow(smallDataTest, 16)
#'
#' @export
colorPlotRainbow <- function(W, numColors) {
  if(class(W)[2] == "DEM") {
    levels <- sort(unique(c(W)))
    size <- length(levels)
    divs <- size/numColors
    gradient <- rev(rainbow(numColors))
    colorVect <- rep(0, size)
    splitLevels <- split(levels, ceiling(seq_along(levels)/divs))
    for(i in 1:numColors) {
      splitLevels[[i]][1:length(splitLevels[[i]])] <- gradient[i]
    }
    splitLevels <- unlist(splitLevels)
    W <- apply(W, 2, rev)
    image(1:nrow(W), 1:ncol(W), W, col=splitLevels, asp = 1, xaxt="n", yaxt="n", bty="n", xlab="", ylab="")
  } else {
    print("El objeto no es de la clase DEM")
  }

}
